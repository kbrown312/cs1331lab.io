---
layout: homework
title: HW6 - Database
---

# Homework 6 - Database

## Introduction

In this assignment you will practice

-   Using [Optional](https://docs.oracle.com/javase/10/docs/api/java/util/Optional.html)
-   Using [Predicate](https://docs.oracle.com/javase/10/docs/api/java/util/function/Predicate.html)
-   Using Lists
-   Using Lambdas, Anonymous classes, and Inner classes

## Problem Description

In this homework, you will create many useful classes for our end goal of making an ATM database!

-   `Transaction`
    -   Keeps track of withdrawals/deposits from/into an account.
-   `Account`
    -   Keeps track of a list of Transactions for a customer.

You will be provided a [TransactionType](./TransactionType.java) enum.

## Solution Description

Classes:

-   `Transaction`

    -   fields:
        -   `TransactionType` type;
        -   `double` amount;
        -   `Optional<String>` comment; (See link above for Optional documentation)
    -   methods:
        -   Getter methods for all fields.
        -   `boolean hasComment()` should return true if comment is not empty, false otherwise.
    -   constructors:
        -   one that takes in `type` and `amount` in that order and assigns them. Sets the `comment` to Optional.empty().
        -   one that takes in `type` and `amount` in that order and assigns them. Also take in a third parameter that is a String that represents that value of the comment. Properly initialize the comment field with this String.

-   `Account`

    -   fields:
        -   `List<Transaction>` pastTransactions
    -   methods:
        -   `Transaction getTransaction(int n)`: returns the nth transaction from pastTransactions. `n` will always be valid.
        -   `List<Transaction> findTransactionsByPredicate(Predicate<Transaction> predicate)`: Returns a list of `Transaction`s from `pastTransactions` filtered by the predicate. If the predicate returns true when a `Transaction` is passed in, keep it. Otherwise filter it out of the returned list. Must not modify `pastTransactions` field. (See link above for documentation on Predicate)
        -   `List<Transaction> getTransactionsByAmount(double amount)`: returns a list of `Transaction`s with an amount field that equals the parameter amount. Must not modify `pastTransactions` field. Must call `findTransactionsByPredicate` with an instance of an **inner class**.
        -   `List<Transaction> getWithdrawals()`: returns a list of `Transaction`s that are withdrawals. Must not modify `pastTransactions` field. Must call `findTransactionsByPredicate` with an instance created with an **anonymous inner class**.
        -   `List<Transaction> getDeposits()`: returns a list of `Transaction`s that are deposits. Must not modify `pastTransactions` field. Must call `findTransactionsByPredicate` with an instance created with a **lambda expression**.
        -   `List<Transaction> getTransactionsWithCommentLongerThan(int length)`: returns a list of `Transaction`s with comments that are longer than `length`. Must not modify `pastTransactions` field. Must call `findTransactionsByPredicate` but you can use whatever technique to create the Predicate that you like.
        -   `List<Transaction> getTransactionsWithComment()`: returns a list of `Transaction`s with a comment that is not empty. Must not modify `pastTransactions` field. Must call `findTransactionsByPredicate` with a **lambda expression or method reference**. Method references aren't taught but they are very useful so feel free to look up how to use them!
        -   Getter for `pastTransactions`.
    -   constructors
        -   Make a constructor that takes in a List of `Transaction`s assign that list to `pastTransactions`.

If there are any classes that you are unfamiliar with (e.g. Predicate or Optional), look them up in Javadocs. Make the visibility of all the methods and fields as you see best fits good programming style.

## Allowed imports

If there is something else you'd like to use, ask on Piazza.

-   Optional
-   Predicate
-   List
-   Any subclass of List

## Running and Testing

You should create your own testing class to verify that your code works (Don't submit it though).

Make sure that all your code can be compiled and ran from the command line.

## Grading

**Submit code that compiles!!!**

Non compiling code will receive an automatic zero.

-   Transaction (25 Points)

    -   Each field is correctly declared (2pts per field)
    -   Constructors (5pts per constructor)
    -   Getter methods (2pt per getter)
    -   hasComment (3pts)

-   Account (75 points)
    -   pastTransactions is correctly declared and assigned (5pts)
    -   `getTransaction()` (10)
    -   `getTransactionsByAmount` (10)
    -   `getWithdrawals` (10)
    -   `getDeposits` (10)
    -   `getTransactionsWithCommentLongerThan` (10)
    -   `getTransactionsWithComment` (10)
    -   `findTransactionsByPredicate` (10)

## Tips and Considerations

**Please start as soon as possible!** The earlier you start this assignment, the more time you will have to think and ask questions. If anything seems confusing, read through the entire description and instructions again. As always, feel free to contact your TAs, post on Piazza, or come in for office hours.

## Javadocs

-   You will need to write Javadoc comments. Not following the Javadoc requirements will count as checkstyle errors.

    -   Every class should have a class level Javadoc that includes `@author <GT Username>`.

    -   Every method should have a Javadoc explaining what the method does and includes any of the following tags if applicable.

        -   `@param <parameter name> <brief description of parameter>`

        -   `@returns <brief description of what is returned>`

        -   `@throws <Exception> <brief explanation of when the given exception is thrown>`

-   See the [CS 1331 Style Guide](https://cs1331.gitlab.io/cs1331-style-guide.html) section on Javadoc comments for examples.

## Checkstyle

You must run checkstyle on your code to check for violations. For each violation the tool finds, you will lose one point on your total grade for this assignment.

For _this homework_, the **checkstyle cap is 100**, meaning you can lose up to **100** points on this assignment due to style errors. As the semester goes on, this cap will increase with each homework and will eventually go away. **Run checkstyle early, and get in the habit of writing style compliant code the first time**. Don't wait until 5 minutes before the deadline to find out that you have 100+ violations.

-   If you encounter trouble running checkstyle, check Piazza for a solution and/or ask a TA as soon as you can!
-   You can run checkstyle on your code by using the jar file found on the course website that includes xml configuration file specifying our checks. **To check the style of your code run** `java -jar checkstyle-6.2.2.jar *.java`.
-   **To check your Javadocs run** `java -jar checkstyle-6.2.2.jar -j *.java`.
-   Note that the command for checking code and the command for checking Javadocs are different. **You will have to run both commands to fully test for style errors.**
-   Javadoc errors are the same as checkstyle errors, as in each one is worth a single point and they are counted towards the checkstyle cap.

## Collaboration

When completing homeworks for CS1331 you may talk with other students about:

-   What general strategies or algorithms you used to solve problems in the homeworks
-   Parts of the homework you are unsure of and need more explanation
-   Online resources that helped you find a solution
-   Key course concepts and Java language features used in your solution

You may **not** discuss, show, or share by other means the specifics of your code, including screenshots, file sharing, or showing someone else the code on your computer, or use code shared by others.

Examples of approved/disapproved collaboration:

-   **Approved**: "Hey, I'm really confused on how we are supposed to implement this part of the homework. What strategies/resources did you use to solve it?"
-   **Disapproved**: "Yo it's 10:40 on Thursday... Can I see your code? I won't copy it directly I promise"

In addition to the above rules, note that it is not allowed to upload your code to any sort of public repository. This could be considered an Honor Code violation, even if it is after the homework is due.

## Submission

Submit your `Transaction` and `Account` files as attachments to the hw6 assignment on Canvas. Be sure to check Piazza for clarifications and updates before submitting. You can submit as many times as you want, so feel free to submit as you make substantial progress on the homework. We only grade your last submission, meaning we will ignore any previous submissions. **Submit every file every time you resubmit**.

As always, **late submissions will not be accepted and non-compiling code will be given a score of 0**. For this reason, we recommend submitting early and then confirming that you submitted ALL of the necessary files by re-downloading your file(s) and compiling/running them.

**Good luck, and have fun!**
