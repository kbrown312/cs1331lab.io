---
layout: default
title: CS 1331 - Office Hours
---

# Office Hours

## Professor Simpkins's Office Hours located in CoC 133

- Monday 15:30 - 17:30
- Other times by appt.

Any time my office door is open, feel free to walk in. Email me if you need to see me, even during office hours since I won't necessarily be in my office or the TA lab if I'm not expecting anyone.

## TA Office Hours, CoC 107

This schedule is subject to change so be sure to double check this page before coming to office hours.

<iframe style="width: 1100px; height: 1200px;" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTxEJNsJ6bkZqN7EU3XqMcPKb4Z5iycsgTFE8V86pGJ62Sqm7eFYqBir5K6NHEpnwSRZgdbqJk4WUiv/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
